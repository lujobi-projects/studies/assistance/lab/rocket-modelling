# Inverted Pendulum proof of concept

Readme incoming

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/lujobi-projects%2Fstudies/assistance/lab/rocket-modelling/master?filepath=Rocket.ipynb)
[![Binder Lab](https://img.shields.io/badge/Jupyter-Lab-blue)](https://mybinder.org/v2/gl/lujobi-projects%2Fstudies/assistance/lab/rocket-modelling/master?filepath=Rocket.ipynb&urlpath=lab/tree/Rocket.ipynb)
[![pipeline status](https://gitlab.com/lujobi-projects/studies/assistance/lab/rocket-modelling/badges/master/pipeline.svg)](https://gitlab.com/lujobi/studies/assistance/lab/rocket-modelling/-/commits/master)
[![pylint](https://gitlab.com/lujobi-projects/studies/assistance/lab/rocket-modelling/-/jobs/artifacts/master/raw/pylint/pylint.svg?job=pylint)](https://gitlab.com/lujobi-projects/studies/assistance/lab/rocket-modelling/-/jobs/artifacts/master/browse/pylint?job=pylint)
[![jobs](https://gitlab.com/lujobi-projects/studies/assistance/lab/rocket-modelling/-/jobs/artifacts/master/raw/public/docs.svg?job=pages)](https://lujobi-projects.gitlab.io/studies/assistance/lab/rocket-modelling/)


**use Python 3.x in order to execute**

## Documentation
Docs can be found [here](https://lujobi-projects.gitlab.io/studies/assistance/lab/rocket-modelling/).

## Development
Crate a virtal environment and source said venv
``` bash
$ virtualenv venv
$ source venv/bin/activate
```

*If your distro doesn't use python3.x as python (e.g. ubuntu) you need to install python 3 the following way*
``` bash
$ sudo apt install python3
$ virtualenv -p /usr/bin/python3.*X* venv #replace X
$ source venv/bin/activate
$ python -V #should display 3.x.x
```

*If the build of slycot fails (it uses fortran), install `python-slycot` via your package-manager and rerun the installation*

Install dependencies #Todo convert into module
``` bash
$ pip install -r requirements.txt
$ pip install jupyterlab
```
Start jupyter:
``` bash
$ jupyter lab
```
