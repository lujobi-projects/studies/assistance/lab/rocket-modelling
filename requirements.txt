#Math
numpy==1.21.4
scipy==1.8.1

#Plotting
matplotlib==3.5.2
ipympl==0.8.4

#control
control==0.9.0
slycot==0.4.0.0

#images/videos
ffmpeg==1.4
opencv-python==4.5.4.60
pillow

#cosmetics
halo==0.0.31
nodejs==0.1.1
tqdm

#needed to build slycot in binder
scikit-build

ipywidgets
jupyter_contrib_nbextensions