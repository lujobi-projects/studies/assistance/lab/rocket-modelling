"""Draws the inverted pendulum according to its state vector"""
import math
import numpy as np
import cv2
import shutil
import os
import uuid
from IPython.display import Image as ImOut

from PIL import Image

if __name__ != '__main__':
    from halo import HaloNotebook as Halo
    from .utils import progress


class Display:
    """Draws the inverted pendulum according to its state vector"""

    def __init__(self, x_span, y_span, max_mt_thrust, max_ht_thrust, *,
                 color_background=None,
                 color_ground=None,
                 color_font=None,
                 color_marks=None,
                 color_rocket=None,
                 color_flare=None
                 ):

        # CAREFUL cv2 ha the colors in BRG not RGB
        self.color_background = color_background if color_background else [
            225, 225, 225]
        self.color_ground = color_ground if color_ground else [19, 69, 139]
        self.color_font = color_font if color_font else [252, 136, 3]
        self.color_marks = color_marks if color_marks else [0, 255, 0]
        self.color_rocket = color_rocket if color_rocket else [0, 0, 0]
        self.color_flare = color_flare if color_flare else [250, 206, 135]
        self.x_span = x_span
        self.y_span = y_span
        self.max_mt_thrust = max_mt_thrust
        self.max_ht_thrust = max_ht_thrust


        # image settings
        self.img_height = 512
        self.img_width = 512
        try:
            shutil.rmtree('out', ignore_errors=True)
        finally:
            os.mkdir('out')

    # pylint: disable=too-many-locals, too-many-statements
    def step(self, state_vec, input_vec, t=None):
        """ state_vec :
                x0 : x - position of the rocket
                x1 : x' - veclocity of the cart
                x2 : y - position of the rocket
                x3 : y' - veclocity of the cart
                x4 : angular velocity of rocket
                x5 : angle of rocket
            input_vec:
                u0 : F_m - Force in Newton
                u1 : a_m - Angle of thruste in rad
                u2 : F_c - Force in Newton
        """
        # Rocket state
        rocket_x = state_vec[0]
        rocket_y = state_vec[2]
        rocket_angle = state_vec[5]

        # Rocket input
        f_m = input_vec[0]  # main thruster percent
        a_m = input_vec[1]  # alpha_m
        f_c = input_vec[2]  # help thruster percent

        # Rocket setting
        l_tot = 60  # Length rocket
        l_m = 10  # Length thrust-COM
        l_l = 5  # Length leg
        l_c = 40 # Length COM - help thrust
        l_mt = l_m*4/5  # Length COM - main thrust nozzle
        w_r = 6 # rocket width

        # some prop consts

        l_ht = w_r * 1.3 # Length x-axis - help thrust nozzle
        com_size = w_r*5/4

        leg_height_const = 0.2
        leg_angle = np.pi / 3
        # thruster
        ang_mt = np.pi/8
        ang_mt_flare = 0.7*ang_mt / np.pi*180  # opening angle of flare in degrees
        ang_ht = np.pi/8
        ang_ht_flare = 0.5*ang_ht / np.pi*180

        # Flare size
        max_m_size = w_r * 10
        max_h_size = max_m_size / 2

        height_reserve = 20

        # calculate vars
        ground_height = self.img_height - height_reserve
        rocket_angle_deg = rocket_angle / np.pi * 180

        def x2x_img(x):
            return int((x + self.x_span) / (2 * self.x_span) * self.img_width)

        def y2y_img(y):
            return self.img_height - height_reserve - int(
                (y) / (self.y_span) * (self.img_height-height_reserve*1.5))

        prop_const = ground_height/self.y_span
        com_x = x2x_img(rocket_x)
        com_y = y2y_img(rocket_y)

        # np.zeros( (512, 512,3), dtype='uint8' )
        image = np.full((self.img_width, self.img_height, 3),
                        self.color_background, dtype='uint8')

        # Ground line
        cv2.line(image, (0, ground_height),
                 (self.img_height, ground_height),
                 self.color_ground, 2)
        # Mark ground line
        for x_d in np.linspace(- self.x_span, self.x_span, 11):
            x = x2x_img(x_d)

            cv2.circle(image, (x, ground_height), 4, self.color_marks, -1)

            cv2.putText(image, str(x_d), (x-15, ground_height+15),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, self.color_font, 1)

        # Mark ground line
        x_mid_cap = int(self.img_width/2)
        cv2.line(image, (x_mid_cap, int(height_reserve*0.5)),
                 (x_mid_cap, ground_height), self.color_ground, 2)
        for y_d in np.linspace(0, self.y_span, 11):
            y = y2y_img(y_d)
            cv2.circle(image, (x_mid_cap, y), 4, self.color_marks, -1)
            if not y_d == 0:
                cv2.putText(image, str(y_d), (x_mid_cap+10, y+7),
                            cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, self.color_font, 1)

        # definition
        #         ___
        #       -     -
        #      /       \
        #     x1 ----- x2
        #     |         |
        #    >|    .    |<
        #     |         |
        #     |         |
        #     |         |
        #     |         |
        #     |         |
        #     |         |
        #     |         |
        #     |   COM   |
        #     |    .    |
        #     |         |
        #     |    .    |
        #     x3 ----- x4
        #    /    / \    \
        #   /             \
        #  /               \
        # x5               x6

        # Rocket body
        upper_height = (l_tot-l_m-l_l) * prop_const
        rocket_width = w_r *prop_const
        upper_diag = (upper_height**2 + rocket_width**2 / 4) ** .5
        upper_angle = np.arctan(rocket_width/2/upper_height)
        lower_height = l_m * prop_const
        lower_diag = (lower_height**2 + rocket_width**2 / 4) ** .5
        lower_angle = np.arctan(rocket_width/2/lower_height)

        x1 = int(com_x - np.sin(upper_angle + rocket_angle)*upper_diag)
        y1 = int(com_y - np.cos(upper_angle + rocket_angle)*upper_diag)
        x2 = int(com_x - np.sin(-upper_angle + rocket_angle)*upper_diag)
        y2 = int(com_y - np.cos(-upper_angle + rocket_angle)*upper_diag)
        x3 = int(com_x + np.sin(-lower_angle + rocket_angle)*lower_diag)
        y3 = int(com_y + np.cos(-lower_angle + rocket_angle)*lower_diag)
        x4 = int(com_x + np.sin(lower_angle + rocket_angle)*lower_diag)
        y4 = int(com_y + np.cos(lower_angle + rocket_angle)*lower_diag)

        pts = np.array(
            [[[x1, y1], [x2, y2], [x4, y4], [x3, y3]]], np.int32)
        cv2.fillPoly(image, pts, self.color_rocket)

        # Cap
        radius = int(rocket_width/2)
        x_mid_cap = int((x1+x2)/2)
        y_mid_cap = int((y1+y2)/2)

        cv2.ellipse(image, (x_mid_cap, y_mid_cap), (int(radius/2), radius),
                    -rocket_angle_deg - 90, 90, -90, self.color_rocket, -1)

        # Landing legs
        length_leg = l_l*prop_const / np.cos(leg_angle)
        x5 = int(x3 + np.sin(-leg_angle + rocket_angle)*length_leg)
        y5 = int(y3 + np.cos(-leg_angle + rocket_angle)*length_leg)
        x6 = int(x4 + np.sin(leg_angle + rocket_angle)*length_leg)
        y6 = int(y4 + np.cos(leg_angle + rocket_angle)*length_leg)

        cv2.line(image, (x3, y3), (x5, y5),
                 self.color_rocket, math.ceil(rocket_width/10))
        cv2.line(image, (x4, y4), (x6, y6),
                 self.color_rocket, math.ceil(rocket_width/10))

        # leg trusses
        x_mid_leg_left = int(x1*leg_height_const + x3*(1-leg_height_const))
        y_mid_leg_left = int(y1*leg_height_const + y3*(1-leg_height_const))
        x_mid_leg_right = int(x2*leg_height_const + x4*(1-leg_height_const))
        y_mid_leg_right = int(y2*leg_height_const + y4*(1-leg_height_const))

        cv2.line(image, (x_mid_leg_left, y_mid_leg_left),
                 (x5, y5), self.color_rocket, math.ceil(rocket_width/15))
        cv2.line(image, (x_mid_leg_right, y_mid_leg_right),
                 (x6, y6), self.color_rocket, math.ceil(rocket_width/15))

        # main thruster
        len_mt = l_mt*prop_const
        rad_mt = int(abs(max_m_size*f_m/self.max_mt_thrust))
        x_mt = int(com_x + np.sin(rocket_angle)*len_mt)
        y_mt = int(com_y + np.cos(rocket_angle)*len_mt)
        x_mt_l = int(x_mt + np.sin(-ang_mt+rocket_angle+a_m)*len_mt*.6)
        y_mt_l = int(y_mt + np.cos(-ang_mt+rocket_angle+a_m)*len_mt*.6)
        x_mt_r = int(x_mt + np.sin(ang_mt+rocket_angle+a_m)*len_mt*.6)
        y_mt_r = int(y_mt + np.cos(ang_mt+rocket_angle+a_m)*len_mt*.6)

        # flare mt
        a_m_deg = a_m * 180 / np.pi
        cv2.ellipse(image, (x_mt, y_mt), (rad_mt, rad_mt),
                    -rocket_angle_deg-a_m_deg+90, -ang_mt_flare, ang_mt_flare, self.color_flare, -1)
        pts = np.array(
            [[[x_mt, y_mt], [x_mt_l, y_mt_l], [x_mt_r, y_mt_r]]], np.int32)
        cv2.fillPoly(image, pts, self.color_rocket)

        # Help thruster
        len_ht = l_c*prop_const
        rad_ht = int(abs(max_h_size*f_c/self.max_ht_thrust))
        # left
        x_ht_l = int(com_x + np.sin(-rocket_angle) *
                     len_ht - np.cos(-rocket_angle)*l_ht)
        y_ht_l = int(com_y - np.cos(-rocket_angle) *
                     len_ht - np.sin(-rocket_angle)*l_ht)
        x_ht_l_d = int(x_ht_l - np.cos(-ang_ht+rocket_angle)*l_ht*1.4)
        y_ht_l_d = int(y_ht_l + np.sin(-ang_ht+rocket_angle)*l_ht*1.4)
        x_ht_l_u = int(x_ht_l - np.cos(ang_ht+rocket_angle)*l_ht*1.4)
        y_ht_l_u = int(y_ht_l + np.sin(ang_ht+rocket_angle)*l_ht*1.4)
        # flare
        if f_c < 0:
            cv2.ellipse(image, (x_ht_l, y_ht_l), (rad_ht, rad_ht),
                        -rocket_angle_deg+180, -ang_ht_flare, ang_ht_flare, self.color_flare, -1)
        pts = np.array(
            [[[x_ht_l, y_ht_l], [x_ht_l_d, y_ht_l_d], [x_ht_l_u, y_ht_l_u]]], np.int32)
        cv2.fillPoly(image, pts, self.color_rocket)

        # right
        x_ht_r = int(com_x + np.sin(-rocket_angle) *
                     len_ht + np.cos(rocket_angle)*l_ht)
        y_ht_r = int(com_y - np.cos(-rocket_angle) *
                     len_ht + np.sin(-rocket_angle)*l_ht)
        x_ht_r_d = int(x_ht_r + np.cos(ang_ht-rocket_angle)*l_ht*1.4)
        y_ht_r_d = int(y_ht_r + np.sin(ang_ht-rocket_angle)*l_ht*1.4)
        x_ht_r_u = int(x_ht_r + np.cos(-ang_ht-rocket_angle)*l_ht*1.4)
        y_ht_r_u = int(y_ht_r + np.sin(-ang_ht-rocket_angle)*l_ht*1.4)
        # flare

        if f_c > 0:
            cv2.ellipse(image, (x_ht_r, y_ht_r), (rad_ht, rad_ht),
                        -rocket_angle_deg, -ang_ht_flare, ang_ht_flare, self.color_flare, -1)
        pts = np.array(
            [[[x_ht_r, y_ht_r], [x_ht_r_d, y_ht_r_d], [x_ht_r_u, y_ht_r_u]]], np.int32)
        cv2.fillPoly(image, pts, self.color_rocket)

        # Center of Gravity
        cv2.circle(image, (com_x, com_y), int(com_size)+1, [255, 255, 255], -1)
        cv2.ellipse(image, (com_x, com_y), (int(com_size), int(com_size)),
                    0, 0, 90, [0, 0, 0], -1)
        cv2.ellipse(image, (com_x, com_y), (int(com_size), int(com_size)),
                    180, 0, 90, [0, 0, 0], -1)

        # # Mark the current angle
        # angle_display = bob_angle % 360
        # if angle_display > 180:
        #     angle_display = -360+angle_display
        # cv2.putText(image,
        #             "theta="+str(np.round(angle_display, 4))+" deg",
        #             (pendulum_hinge_x-15, pendulum_hinge_y-15),
        #             cv2.FONT_HERSHEY_COMPLEX_SMALL,
        #             0.8,
        #             self.color_font,
        #             1)

        # Display on top
        if t is not None:
            cv2.putText(image, "t="+str(np.round(t, 4))+"sec", (15, 15),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, self.color_font, 1)
            cv2.putText(image, "ANG="+str(np.round(rocket_angle_deg, 4))+" degrees",
                        (15, 35), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, self.color_font, 1)
            cv2.putText(image, "POS="+str(np.round(rocket_x, 4))+str(np.round(rocket_y, 4))+" m",
                        (15, 55), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, self.color_font, 1)

        return image

    def save_vid_gifs(self, sol, ipt, filename):
        """Saves the videos generated from the supplied states

            state_vec :
                x0 : x - position of the rocket
                x1 : x' - veclocity of the cart
                x2 : y - position of the rocket
                x3 : y' - veclocity of the cart
                x4 : angular velocity of rocket
                x5 : angle of rocket
            input_vec:
                u0 : F_m - Force in Newton
                u1 : a_m - Angle of thruste in rad
                u2 : F_c - Force in Newton
        

        :param sol: state vector for each point in time, see state_vec
        :type sol: np.matrix
        :param ipt: input vector for each point in time, see input_vec
        :type ipt: np.matrix
        :param filename: name under which the file has to be saved
        :type filename: string
        """        
        y = sol.y.T
        
        rendered = []
        for i in progress.log_progress(range(len(y)), every=1, name='Render Progress'):
            rendered.append(
                Image.fromarray( # Create a PIL image
                    cv2.cvtColor( # Convert from OpenCV to PIL color space
                        self.step(y[i], ipt.T[i]), # render step
                        cv2.COLOR_BGR2RGB), # BGR to RGB space
                     'RGB')
                )
        # Render the gif from the array of images 
        rendered[0].save(filename, save_all=True,optimize=False, append_images=rendered[1:], loop=0)

    def play_video(self, sol, ipt):
        """Saves at a random location and returns a playable object

            state_vec :
                x0 : x - position of the rocket
                x1 : x' - veclocity of the cart
                x2 : y - position of the rocket
                x3 : y' - veclocity of the cart
                x4 : angular velocity of rocket
                x5 : angle of rocket
            input_vec:
                u0 : F_m - Force in Newton
                u1 : a_m - Angle of thruste in rad
                u2 : F_c - Force in Newton
        

        :param sol: state vector for each point in time, see state_vec
        :type sol: np.matrix
        :param ipt: input vector for each point in time, see input_vec
        :type ipt: np.matrix

        :return: displayable gif 
        :rtype: IPython.display.Image
        """        
        filename = f'out/{uuid.uuid4()}.gif' 
        self.save_vid_gifs(sol, ipt, filename)
        return ImOut(filename=filename)
    

if __name__ == '__main__':
    # for developing the image
    DISP = Display(5, 100, 100, 100)
    img = DISP.step([0, 0, 20, 0, 0, 0*np.pi/6], [100, 100, 100])
    cv2.imshow('testi', img)

    cv2.waitKey(0)
    cv2.destroyAllWindows()
