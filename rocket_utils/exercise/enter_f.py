""" Exercise to find the sytem equations

"""
from IPython.display import Latex, clear_output, Markdown, display
import ipywidgets as widgets
from rocket_utils.input import InputMatrix, random_unit_test_matrix, text_inputs_to_matrix


def exercise():
    """ Returns a displayable Exercise including unittesting the solutions

    :return: displayable Exercise as ipy widget
    :rtype: ipywidgets.VBox
    """    
    solution = [['-sin(alpha_m+alpha)*F_m/m - cos(alpha)*F_c/m'], ['cos(alpha_m+alpha)*F_m/m - sin(alpha)*F_c/m'], ['-l_m/theta*sin(alpha_m)*F_m + l_c/theta*F_c'], ['omega']]

    var = ['m', 'alpha', 'alpha_m', 'g', 'l_m', 'l_c', 'theta', 'F_m', 'F_c', 'omega']


    latex_desc = r"""Finde die Funktion
$$F(x) = \begin{bmatrix}
    \dot x \\
    \dot y \\
    \dot \omega \\
    \dot \alpha \\
\end{bmatrix}$$

Verwende die folgenden Variablennamen für die Variablen:

*    $m$: `m`
*    $\alpha$: `alpha`
*    $\alpha_m$: `alpha_m`
*    $g$: `g`
*    $\ell_m$: `l_m`
*    $\ell_c$: `l_c`
*    $\Theta$: `theta`
*    $F_m$: `F_m`
*    $F_c$: `F_c`
*    $\omega$: `omega`


"""

    latex_newton = r"""
$$F(x) = \begin{bmatrix}
    \dot x \\
    \dot y \\
    \dot \omega \\
    \dot \alpha \\
\end{bmatrix} = 
\begin{bmatrix}
    -\frac{1}{m}\sin(\alpha+\alpha_m)\cdot F_m - \frac{1}{m}\cos(\alpha)\cdot F_c \\
    \frac{1}{m}\cos(\alpha+\alpha_m)\cdot F_m-\frac{1}{m}\sin(\alpha)\cdot F_c-g \\
    -\frac{\ell_m}{\Theta}\sin(\alpha_m)\cdot F_m + \frac{\ell_c}{\Theta}\cdot F_c \\
    \omega \\
\end{bmatrix}$$

Um `y` und `x` bestimmen zu können, wurde die Funktion um zwei Zustände erweitert:

$$u=\begin{bmatrix}
    F_m\\
    \alpha_m \\
    F_c\\
\end{bmatrix}, 
z = 
\begin{bmatrix}
    z_1 \\
    z_2 \\
    z_3 \\
    z_4 \\
    z_5 \\
    z_6 \\
\end{bmatrix} = 
\begin{bmatrix}
    x \\
    \dot x \\
    y \\
    \dot y \\
    \omega \\
    \alpha
\end{bmatrix} \rightarrow \dot z = 
\begin{bmatrix}
    \dot z_1 \\
    \dot z_2 \\
    \dot z_3 \\
    \dot z_4 \\
    \dot z_5 \\
    \dot z_6 \\
\end{bmatrix} =
\begin{bmatrix}
    \dot x \\
    \ddot x \\
    \dot y \\
    \ddot y \\
    \dot\omega \\
    \dot\alpha
\end{bmatrix} =
\begin{bmatrix}
    z_2 \\
    -\frac{1}{m}\sin(z_6+u_2)\cdot u_1 - \frac{1}{m}\cos(z_6)\cdot u_3\\
    z_4 \\
    \frac{1}{m}\cos(z_6+u_2)\cdot u_1-\frac{1}{m}\sin(z_6)\cdot u_3-g \\
    -\frac{\ell_m}{\Theta}\sin(u_2)\cdot u_1 + \frac{\ell_c}{\Theta}\cdot u_3 \\
    \omega \\
\end{bmatrix}$$
"""


    f_grid, f_items = InputMatrix(4, 1, 500)

    btn = widgets.Button(
        description='Check input',
        disabled=False,
        button_style='info',
        tooltip='Click me',
        icon='check' # (FontAwesome names without the `fa-` prefix)
    )

    desc = widgets.Output()
    with desc:
        desc.clear_output()
        display(Markdown(latex_desc))

    output = widgets.Output()
    def on_button_clicked(b):
        with output:
            output.clear_output()
            if random_unit_test_matrix(solution, text_inputs_to_matrix(f_items), var):
                display(Latex(f'All correct!! \n\n Your input formatted into latex: \n\n {latex_newton}'))
            else:
                print('try again')
            
    btn.on_click(on_button_clicked)

    res = widgets.VBox([desc, f_grid, btn,output])

    return res