""" Exercise to enter the sytem matrices expanded in order to account for the position of the rocket

"""

from IPython.display import Latex, clear_output, Markdown, display
import ipywidgets as widgets
from rocket_utils.input import InputMatrix, random_unit_test_matrix, text_inputs_to_matrix


def exercise():
    """ Returns a displayable Exercise including unittesting the solutions

    :return: displayable Exercise as ipy widget
    :rtype: ipywidgets.VBox
    """    
    a_solution = [
        ['0', '1', '0', '0', '0', '0'],
        ['0', '0', '0', '0', '0', '-g'],
        ['0', '0', '0', '1', '0', '0'],
        ['0', '0', '0', '0', '0', '0'],
        ['0', '0', '0', '0', '0', '0'],
        ['0', '0', '0', '0', '1', '0'],
    ]
    b_solution = [
        ['0', '0', '0'],
        ['0', '-g', '-1/m'],
        ['0', '0', '0'],
        ['1/m', '0', '0'],
        ['0', '-(l_m * m * g)/theta', 'l_c/theta'],
        ['0', '0', '0'],
    ]

    var = ['m', 'alpha', 'alpha_m', 'g', 'l_m', 'l_c', 'theta', 'F_m', 'F_c', 'omega']


    latex_desc = r"""
Linearisieren Sie dieses System $\dot x = A\cdot x + B\cdot u$
    """

    latex_solution = r"""
    $$
\begin{bmatrix}
    \dot x_1 \\
    \dot x_2 \\
    \dot x_3 \\
    \dot x_4 \\
    \dot x_5 \\
    \dot x_6 \\
\end{bmatrix} = 
\begin{bmatrix}
    0 & 1 & 0 & 0 & 0 & 0\\
    0 & 0 & 0 & 0 & 0 & -g\\
    0 & 0 & 0 & 1 & 0 & 0\\
    0 & 0 & 0 & 0 & 0 & 0\\
    0 & 0 & 0 & 0 & 0 & 0\\
    0 & 0 & 0 & 0 & 1 & 0\\
\end{bmatrix} \cdot  
\begin{bmatrix}
    x_1 \\
    x_2 \\
    x_3 \\
    x_4 \\
    x_5 \\
    x_6 \\
\end{bmatrix} + 
\begin{bmatrix}
    0 & 0 & 0\\
    0 & -g & -\frac{1}{m}\\
    0 & 0 & 0\\
    \frac{1}{m} & 0 & 0\\
    0 & -\frac{\ell_m\cdot m\cdot g}{\Theta} & \frac{\ell_c}{\Theta}\\
    0 & 0 & 0\\
\end{bmatrix} \cdot  
\begin{bmatrix}
    u_1 \\
    u_2 \\
    u_3 \\
\end{bmatrix}
$$
"""



    a_grid, a_items = InputMatrix(6, 6, 500)
    b_grid, b_items = InputMatrix(6, 3, 500)

    btn = widgets.Button(
        description='Check input',
        disabled=False,
        button_style='info',
        tooltip='Click me',
        icon='check' # (FontAwesome names without the `fa-` prefix)
    )

    desc = widgets.Output()
    with desc:
        desc.clear_output()
        display(Markdown(latex_desc))

    output = widgets.Output()
    def on_button_clicked(b):
        with output:
            output.clear_output()
            if random_unit_test_matrix(a_solution, text_inputs_to_matrix(a_items), var) and random_unit_test_matrix(b_solution, text_inputs_to_matrix(b_items), var):
                display(Latex(f'All correct!! \n\n Your input formatted into latex: \n\n {latex_solution}'))
            else:
                print('try again')
            
    btn.on_click(on_button_clicked)

    res = widgets.VBox([desc, widgets.Label('A'), a_grid, widgets.Label('B'), b_grid, btn, output])

    return res