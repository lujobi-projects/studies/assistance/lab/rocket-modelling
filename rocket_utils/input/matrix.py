import ipywidgets as widgets
import random
import numpy as np

from math import sin, cos

def InputMatrix(height, width, w=300, h=300):
    """Generates a matrix consisting of pywidget input fields

    :param height: count of cells in vertical direction
    :type height: int
    :param width: count of cells in horizontal direction
    :type width: int
    :param w: width of the matrix in pixels, defaults to 300
    :type w: int, optional
    :param h: height of the matrix in pixels, defaults to 300
    :type h: int, optional
    :return: (displayable Gridlayout, array of Text inputs preserving the matrix size)
    :rtype: (ipywidgets.GridspecLayout, array[ipywidgets.Text])
    """    
    grid = widgets.GridspecLayout(height, width, height='auto', width=f'{w}px')
    val = []
    for i in range(height):
        tmp = []
        for j in range(width):
            text = widgets.Text('0', layout=widgets.Layout(width=f'{int(w/width/1.2)}px'))
            tmp.append(text)
            grid[i, j] = text
        val.append(tmp)
    return grid, val

def text_inputs_to_matrix(ipt):
    """Converts the array of ipywidgets.Text to an array of strings

    :param ipt: array of text inputs
    :type ipt: array[ipywidgets.Text]
    :return: array of all inputed texts
    :rtype: array[string]
    """    
    res = []
    for row in ipt:
        if type(row) is list:
            tmp = []
            for cell in row:
                tmp.append(cell.value)
            res.append(tmp)
        else:
            res.append(row.value)
    return res


def eval_matrix(matrix, var):
    """Evaluates a matrix of strings by its mathematical representation.

    Allowed functions: are all arithmetic functions and sin, cos


    :param matrix: matrix containing strings of mathematical functions to be evaluated
    :type matrix: array[string]
    :param var: dictionary of all variables used in the entered functions
    :type var: dict
    :return: array containing results from the evaluated functions
    :rtype: array[float]
    """    
    vars().update(var) # update local variables with the ones passed to var
    res = np.zeros_like(matrix, dtype=np.float64)
    for row_ind, row in enumerate(matrix):
        for col_ind, cell in enumerate(row):
            try:
                res[row_ind][col_ind] = eval(cell)
            except Exception as err:
                print('Error evaluating the following expression: ' + cell + '\n error: ' + str(err))
            
    return res

def unit_test_matrix(ipt, sol, var):
    """ Performs a unittest of the two supplied matrices and returns true if solution within 1E-4

    :param ipt: matrix containing strings of mathematical functions to be evaluated
    :type ipt: array[string]
    :param sol: matrix containing strings of mathematical functions to be evaluated
    :type sol: array[string]
    :param var: dictionary of all variables used in the entered functions
    :type var: dict
    :return: true if the solutions are within 1E-4
    :rtype: bool
    """
    ipt_eval = eval_matrix(ipt, var)
    sol_eval = eval_matrix(sol, var)
    return np.allclose(ipt_eval, sol_eval, rtol=1E-4)

def random_unit_test_matrix(ipt, sol, var_names, no_test=5, var_range=(-100, 100)):
    """plugs in random numbers for the variables and compares the output of the matrices ipt and sol

    :param ipt: matrix containing strings of mathematical functions to be evaluated
    :type ipt: array[string]
    :param sol: matrix containing strings of mathematical functions to be evaluated
    :type sol: array[string]
    :param var_names: array containing all variable names
    :type var_names: array[string]
    :param no_test: number of unittest to be performed, defaults to 5
    :type no_test: int, optional
    :param var_range: range in which the variables are chosen, defaults to (-100, 100)
    :type var_range: tuple, optional
    :return: boolean whethter the test passed or failed
    :rtype: boolean
    """
    for _ in range(no_test):
        var = {}
        for v in var_names:
            var[v] = random.randrange(*var_range)
        if not unit_test_matrix(ipt, sol, var):
            print(f'error testing your input is \n\n {eval_matrix(ipt, var)}, but should \n\n {eval_matrix(sol, var)} \n\n using the following variables: \n\n {var}')
            return False
    return True
