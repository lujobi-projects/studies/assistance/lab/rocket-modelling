"""
Physical System of a pendulum
"""

from scipy.integrate import solve_ivp
import numpy as np
import control as ct

class System:
    """ Class simlating the physical System, containing the respective transfer functions and differential equations. ** currently damp_rot and damp_air are not used **
    """ 

    def __init__(self, *,
                 m=1,
                 mom_inert=1,
                 g=9.81,
                 l_m=1.5,
                 l_c=10,
                 damp_rot=0.05,
                 damp_air=0.05,
                 f=lambda t: np.zeros(3)
                 ):
        """Simulates Rocket physics

        :param m: mass of the rocket in kg, defaults to 1
        :type m: int, optional
        :param mom_inert: moment of inertia in kg m^2, defaults to 1
        :type mom_inert: float, optional
        :param g:gravitational constant, defaults to 9.81
        :type g: float, optional
        :param l_m: distance of point of attack of main thruster from center of mass, defaults to 1.5
        :type l_m: float, optional
        :param l_c: distance of point of attack of control thruster from center of mass, defaults to 10
        :type l_c: float, optional
        :param damp_rot: damping coefficient of the rotation, defaults to 0.05
        :type damp_rot: float, optional
        :param damp_air: damping coefficient of the pendulum, defaults to 0.05
        :type damp_air: float, optional
        :param f: function for all inputs [F_m, alpha_m, F_c] (t), defaults to lambdat:np.zeros(3)
        :type f: function, optional
        """

        self.m = m
        self.mom_inert = mom_inert
        self.g = g
        self.l_m = l_m
        self.l_c = l_c
        self.damp_rot = damp_rot
        self.damp_air = damp_air
        self.f = f

    @staticmethod
    # pylint: disable=too-many-arguments
    def __dz_dt(t, z, m, mom_inert, g, l_m, l_c, f):  # damp_rot, damp_air, f):
        _, v_x, _, v_y, omega, alpha = z
        f_m, alpha_m, f_c = f(t)

        dz_dt_arr = np.zeros(6)
        dz_dt_arr[0] = v_x
        dz_dt_arr[1] = 1/m * (-np.sin(alpha+alpha_m) *
                              f_m - np.cos(alpha)*f_c)  # - v_x*damp_air
        dz_dt_arr[2] = v_y
        dz_dt_arr[3] = 1/m * (np.cos(alpha+alpha_m)*f_m -
                              np.sin(alpha)*f_c) - g  # - v_y*damp_air
        dz_dt_arr[4] = 1/mom_inert * \
            (-l_m*np.sin(alpha_m)*f_m + l_c*f_c)  # - omega*damp_rot
        dz_dt_arr[5] = omega

        return dz_dt_arr

    def solve_ode(self, time_span, z_0, *, t_eval):
        """solves the exact system ODE

        z being an array [x, v_x, y, v_y, omega, alpha]

        :param time_span: [t_start, t_end], tuple of time start and time end of the sumulation
        :type time_span: (float, float)
        :param z_0: initial state np array of size 6
        :type z_0: np.array
        :param t_eval: optional times at which the solution of the ODE is returned
        :type t_eval: np.array, optional
        :return: solution of ODE for all states
        :rtype: np.matrix
        """        

        def wrapper(t, z):
            return self.__dz_dt(t, z, self.m, self.mom_inert, self.g, self.l_m, self.l_c, self.f)
            # , self.damp_rot, self.damp_air, self.f)
        return solve_ivp(wrapper, time_span, z_0, t_eval=t_eval, dense_output=True, max_step=0.01)

    def state_space(self, C=None):
        """returns the state space representation of the rocket

        :param C: The C-Matrix, defaults to None
        :type C: np.matrix, optional
        :return: State space representation of the Rocket
        :rtype: ControlSystems.ss
        """        
        C = C or np.array(
            [[1, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0], [0, 0, 0, 0, 0, 1]])

        A = np.array(
            [[0, 1, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, -self.g],
             [0, 0, 0, 1, 0, 0],
             [0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 1, 0]])
        B = np.array(
            [[0, 0, 0],
             [0, -self.g, -1/self.m],
             [0, 0, 0],
             [1/self.m, 0, 0],
             [0, -self.l_m*self.m*self.g/self.mom_inert, self.l_c/self.mom_inert],
             [0, 0, 0]])
        D = np.zeros((len(C), 3))

        return ct.ss(A, B, C, D)

    def transfer_function(self, C=None):
        """returns the transfer function of the rocket

        :param C: The C-Matrix, defaults to None
        :type C: np.matrix, optional
        :return: Transfer function of the Rocket
        :rtype: ControlSystems.tf
        """        
        return ct.ss2tf(self.state_space(C))

